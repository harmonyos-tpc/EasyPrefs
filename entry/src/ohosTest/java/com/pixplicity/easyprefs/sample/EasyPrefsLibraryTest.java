/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pixplicity.easyprefs.sample;

import com.pixplicity.easyprefs.library.Prefs;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.data.preferences.Preferences;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class EasyPrefsLibraryTest {
    private final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();

    @Test
    public void testPref() {
        try {
            Preferences pref = Prefs.getPreferences();
            assertNotNull("Preferences is initialized", pref);
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefBoolean() {
        try {
            Prefs.putBoolean("key_bool", true);
            assertTrue("Preferences tested boolean value", Prefs.getBoolean("key_bool"));
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefString() {
        try {
            Prefs.putString("key_string", actualBundleName);
            assertTrue("Preferences tested string value", Prefs.getString("key_string")
                    .equalsIgnoreCase(actualBundleName));
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefInt() {
        try {
            Prefs.putInt("key_int", 100);
            assertEquals("Preferences tested int value", 100, Prefs.getInt("key_int"));
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefLong() {
        try {
            Prefs.putLong("key_long", 100);
            assertEquals("Preferences tested long value", 100, Prefs.getLong("key_long"));
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testDouble() {
        try {
            Prefs.putDouble("key_double", 100);
            assertEquals("Preferences tested double value", 100, Prefs.getDouble("key_double"), 0.0);
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefFloat() {
        try {
            Prefs.putFloat("key_float", 100);
            assertEquals("Preferences tested float value", 100, Prefs.getFloat("key_float"), 0.0);
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefStringSet() {
        Set<String> stringSet = new HashSet<>();

        stringSet.add("openharmony");
        stringSet.add("project");
        stringSet.add("created");
        stringSet.add("EasyPrefs");
        stringSet.add("application");

        try {
            Prefs.putStringSet("key_string_set", stringSet);
            assertEquals("Preferences tested StringSet value", stringSet, Prefs.getStringSet("key_string_set", new HashSet<>()));
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefOrderedStringSet() {
        Set<String> stringSet = new HashSet<>();

        stringSet.add("openharmony");
        stringSet.add("project");
        stringSet.add("created");
        stringSet.add("EasyPrefs");
        stringSet.add("application");

        try {
            Prefs.putOrderedStringSet("key_order_string_set", stringSet);
            assertEquals("Preferences tested ordered StringSet value", stringSet, Prefs.getOrderedStringSet("key_order_string_set", new HashSet<>()));
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefRemove() {
        try {
            Prefs.putFloat("key_float", 100);
            Prefs.remove("key_float");
            Preferences pref = Prefs.getPreferences();
            assertFalse("Key is removed from preferences", pref.hasKey("key_float"));
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefContains() {
        try {
            Prefs.putFloat("key_float", 100);
            assertTrue("Key is presented in preferences", Prefs.contains("key_float"));
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefEdit() {
        try {
            Preferences prefs = Prefs.edit();
            assertNotNull("preferences can be edited", prefs);
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }

    @Test
    public void testPrefClear() {
        try {
            Preferences prefs = Prefs.clear();
            assertNotNull("preferences cleared", prefs);
        } catch (RuntimeException e) {
            assertTrue("Pref is not initialized in MyApplication",true);
        }
    }
}