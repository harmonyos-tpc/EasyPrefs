/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pixplicity.easyprefs.sample.slice;

import com.pixplicity.easyprefs.library.Prefs;
import com.pixplicity.easyprefs.library.TextUtils;
import com.pixplicity.easyprefs.sample.MainAbility;
import com.pixplicity.easyprefs.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.components.Button;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD000F00, MainAbility.class.getSimpleName());

    public static final String SAVED_TEXT = "saved_text";
    public static final String SAVED_NUMBER = "saved_number";
    private static final String FROM_INSTANCE_STATE = " : from instance state";

    private TextField et_text, et_number;
    private Button bt_save_text, bt_save_number, bt_force_close;
    private Text tv_saved_text, tv_saved_number;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_main, null, false);
        et_text = (TextField) rootLayout.findComponentById(ResourceTable.Id_et_text);
        et_number = (TextField) rootLayout.findComponentById(ResourceTable.Id_et_number);
        bt_save_text = (Button) rootLayout.findComponentById(ResourceTable.Id_bt_save_text);
        bt_save_number = (Button) rootLayout.findComponentById(ResourceTable.Id_bt_save_number);
        bt_force_close = (Button) rootLayout.findComponentById(ResourceTable.Id_bt_force_close);
        tv_saved_text = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_saved_text);
        tv_saved_number = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_saved_number);

        bt_save_text.setClickedListener(this);
        bt_save_number.setClickedListener(this);
        bt_force_close.setClickedListener(this);

        // get the saved String from the preference by key, and give a default value
        // if Prefs does not contain the key.
        try {
            String s = Prefs.getString(SAVED_TEXT, getString(ResourceTable.String_not_found));
            HiLog.info(LABEL_LOG, String.format(Locale.ROOT, "%s", s));
            updateText(s);

            double d = Prefs.getDouble(SAVED_NUMBER, -1.0);
            updateNumber(d, false);
        } catch(IllegalArgumentException e) {
            HiLog.error(LABEL_LOG, String.format(Locale.ROOT, "%s", e.getMessage()));
        }

        super.setUIContent(rootLayout);
    }

    private void updateText(String s) {
        String text = String.format(getString(ResourceTable.String_saved_text), s);
        tv_saved_text.setText(text);
    }

    private void updateNumber(double d, boolean fromInstanceState) {
        String text = String.format(getString(ResourceTable.String_saved_number), d, fromInstanceState ? FROM_INSTANCE_STATE : "");
        tv_saved_number.setText(text);
    }

    @Override
    public void onClick(Component component) {
        Intent intent;

        switch (component.getId()) {
            case ResourceTable.Id_bt_save_text:
                String text = et_text.getText();
                if (!TextUtils.isEmpty(text)) {
                    // one liner to save the String.
                    HiLog.info(LABEL_LOG, String.format(Locale.ROOT, "%s", Prefs.contains("key_order_string_set")));
                    try {
                        Prefs.putString(SAVED_TEXT, text);
                        updateText(text);
                        showToast("Saved");
                    } catch(IllegalArgumentException e) {
                        HiLog.error(LABEL_LOG, String.format(Locale.ROOT, "%s", e.getMessage()));
                    }
                } else {
                    showToast("trying to save a text with lenght 0");
                }
                break;
            case ResourceTable.Id_bt_save_number:
                HiLog.info(LABEL_LOG, String.format(Locale.ROOT, "%s", et_number.getText()));
                text = et_number.getText();
                if (!TextUtils.isEmpty(text)) {
                    try {
                        double d = Double.parseDouble(text);
                        Prefs.putDouble(SAVED_NUMBER, d);
                        updateNumber(d, false);
                        showToast("Saved");
                    } catch(IllegalArgumentException e) {
                        HiLog.error(LABEL_LOG, String.format(Locale.ROOT, "%s", e.getMessage()));
                    }
                } else {
                    showToast("trying to save a number with lenght 0");
                }
                break;
            case ResourceTable.Id_bt_force_close:
                terminateAbility();
                break;
            default:
                throw new IllegalArgumentException(
                        "Did you add a new button forget to listen to view ID in the onclick?");
        }
    }

    private void showToast(String text) {
        ToastDialog t = new ToastDialog(MainAbilitySlice.this);
        t.setText(text);
        t.setDuration(100000);
        t.show();
    }
}