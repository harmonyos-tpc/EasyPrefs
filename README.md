# EasyPrefs


## Introduction
A small library containing a wrapper/helper for the Preferences of openharmoy.


## Usage Instruction
With this library you can initialize the Preference inside the onInitialize of the MyApplication class of your app.

```Java
public class MyApplication extends AbilityPackage {

    @Override
    public void onInitialize() {
        super.onInitialize();

        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(this)
                .setMode(AbilityContext.MODE_PRIVATE)
                .setPrefsName(getBundleName())
                .setUseDefaultSharedPreference(true)
                .build();
    }
}
```

After initialization, you can use simple one-line methods to save values to the Preferences anywhere in your app, such as:

- `Prefs.putString(key, string)`
- `Prefs.putLong(key, long)`
- `Prefs.putBoolean(key, boolean)` 

Retrieving data from the Preferences can be as simple as:

    String data = Prefs.getString(key, default value)

If the Preferences contains the key, the string will be obtained, otherwise the method returns the default string provided. No need for those pesky `hasKey()` or `data != null` checks!

For some examples, see the sample App.

### Bonus feature: ordered sets

The default implementation of `getStringSet` on openharmoy **does not preserve the order of the strings in the set**. For this purpose, EasyPrefs adds the methods:

    void Prefs.putOrderedStringSet(String key, Set<String> value);

and

    Set<String> Prefs.getOrderedStringSet(String key, Set<String> defaultValue);

which internally use Java's LinkedHashSet to retain a predictable iteration order.


## Installation tutorial
1. Add following har in entry libs folder:
        library.har
   Add the following code to the gradle of the entry:
        implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

2. Add the following code to the gradle of the entry:
		implementation project(path: ':library')
3. For using EasyPrefs from remote repository in separate application, add the below dependency in "entry" build.gradle.
   
   Modify entry build.gradle as below :
   
   ```
   dependencies {
     implementation 'io.openharmony.tpc.thirdlib:EasyPrefs:1.0.0'
   }
   ```
## License

```
Copyright 2014 Pixplicity (http://pixplicity.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```